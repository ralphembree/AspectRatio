#!/usr/bin/python
import argparse

parser = argparse.ArgumentParser(
    description="Give information for aspect ratios",
    epilog="""With the -W option, give the height required to reach that aspect ratio
With the -H option, give the width required to reach that aspect ratio
With -W and -H options, give the ammount of chopping required in pixels""",
    formatter_class=argparse.RawTextHelpFormatter
)

parser.add_argument('-u', '--usage', action='help', help='show this help message and exit')
parser.add_argument('width', help='width of original image', type=int)
parser.add_argument('height', help='height of original image', type=int)
parser.add_argument('new-width', help='width of new image', type=int, nargs='?')
parser.add_argument('new-height', help='height of new image', type=int, nargs='?')
parser.add_argument('-W', '--new-width', help='width of new image', type=int)
parser.add_argument('-H', '--new-height', help='height of new image', type=int)

args = vars(parser.parse_args())
new_width = args['new_width']
if new_width is None:
    new_width = args['new-width']
elif args['new-width'] is not None:
    parser.error("Can't set new-width two ways")

new_height = args['new_height']
if new_height is None:
    new_height = args['new-height']
elif args['new-height'] is not None:
    parser.error("Can't set new-height two ways")

width = args['width']
height = args['height']

if new_width is not None:
    if new_height is not None:
        if (new_width / float(new_height)) > (width / float(height)):
            needed = new_width - ((width / float(height)) * new_height)
            if round(needed):
                print("%d from the sides" % round(needed))
        else:
            needed = new_height - ((height / float(width)) * new_width)
            if round(needed):
                print("%d from the top and bottom" % round(needed))
        if not needed:
            print("Looks good!")
        elif not round(needed):
            print("Good enough.")
    else:
        print("%dx%d" % (round(new_width), round((height / float(width)) * new_width)))
else:
    if new_height is None:
        parser.error("new-width or new-height required")
    else:
        print("%dx%d" % ((width / float(height)) * new_height, new_height))
