# AspectRatio
Given the width and height of an image and the width and/or height for a scaled image, give relevant information.


Required positional arguments are `width` and `height` which are the original width and height of the image.  `new-width` and `new-height` are the desired width and height of the scaled image.  Only one is necessary and they can be given in two ways: as position arguments, or with the `-W` and `-H` switches. (`-W` stands for `--new-width` and `-H` stands for `--new-height`)  If one is given, the dimensions of the scaled image are given using that number and the aspect-ratio of the original image.  If both are given, it returns the number of pixels that should be cropped to get the aspect-ratio of the original image.

## Installation

* Download the file: `wget https://raw.githubusercontent.com/ralphembree/AspectRatio/master/aspect.py`
* Make it executable: `chmod +x aspect.py`
* Move it to `/usr/bin`: `sudo mv aspect.py /usr/bin/aspectratio`

## Examples

Find the width required to scale a 40x50 image to a height of 500:

    $ aspectratio 40 50 -H 500
    400x500

Find the height required to scale a 40x50 image to a width of 300

    $ aspectratio 40 50 300
    300x375

Give amount of cropping required after scaling a 400x500 image to 300x200

    $ aspectratio 400 500 300 200
    140 from the sides

Give amount of cropping required after scaling a 400x500 image to 40x50

    $ aspectratio 400 500 40 50
    Looks good!

Give amount of cropping required after scaling a 4x5 image to 3x4

    $ aspectratio 4 5 3 4
    Good enough.

Help:

    $ aspectratio --help
    usage: aspect.py [-h] [-u] [-W NEW_WIDTH] [-H NEW_HEIGHT]
                     width height [new-width] [new-height]

    Give information for aspect ratios

    positional arguments:
      width                 width of original image
      height                height of original image
      new-width             width of new image
      new-height            height of new image

    optional arguments:
      -h, --help            show this help message and exit
      -u, --usage           show this help message and exit
      -W NEW_WIDTH, --new-width NEW_WIDTH
                            width of new image
      -H NEW_HEIGHT, --new-height NEW_HEIGHT
                            height of new image

    With the -W option, give the height required to reach that aspect ratio
    With the -H option, give the width required to reach that aspect ratio
    With -W and -H options, give the amount of chopping required in pixels
